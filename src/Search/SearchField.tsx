import React, { useEffect, useState } from 'react';
import SearchResultList from './SearchResultList';
import brainData from '../../data/Brain.json';
import {
  Box,
  Button,
  FormControlLabel,
  Switch,
  TextField,
} from '@mui/material';
import { isArray } from '../Utils/utils';

export interface Json {
  [x: string]: string | number | boolean | Date | Json | JsonArray;
}

export type JsonArray = Array<
  string | number | boolean | Date | Json | JsonArray
>;

export interface SearchResult {
  path: Array<string>;
  value: Json;
  displayPath?: Array<string>;
}

const SearchField = () => {
  const [searchTerm, setSearchTerm] = useState<Array<string>>([]);
  const [brain, setBrain] = useState<JsonArray>([]);
  const [searchResults, setSearchResults] = useState<Array<SearchResult>>([]);
  const [switchChecked, setSwitchChecked] = useState(true);
  const [objectSearchChecked, setObjectSearchChecked] = useState(false);

  useEffect(() => {
    setBrain(JSON.parse(JSON.stringify(brainData)));
    setSearchResults([]);
  }, []);

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      BeginSearch();
    }
  };
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const input = event.currentTarget.value.toLowerCase();
    setSearchTerm(input.split(';').map((term) => term.trim()));
  };
  const handleSwitchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSwitchChecked(event.target.checked);
  };
  const handleObjectSearchChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setObjectSearchChecked(event.target.checked);
  };

  // Retrieves the highest level findings of the searchTerm
  const BeginSearch = () => {
    const results = SearchBrain(brain, [], []);
    setSearchResults(results);
  };
  const ClearResults = () => setSearchResults([]);
  const DisplayAll = () => setSearchResults(SearchBrain(brain, [], [], []));
  const SearchBrain = (
    item: JsonArray,
    path: Array<string>,
    displayPath: Array<string>,
    search: Array<string> = searchTerm
  ) => {
    let results: SearchResult[] = [];
    // Loop each given object
    for (const key in item) {
      const value = item[key];
      const newPath = path.concat(key);
      let newDisplayPath;
      // @ts-ignore
      const nameIfExists = value['Name'];
      if (isArray(item)) {
        newDisplayPath = nameIfExists
          ? displayPath.concat(nameIfExists)
          : displayPath;
      } else {
        newDisplayPath = displayPath.concat(key);
      }

      const itemType = typeof value;
      switch (itemType) {
        case 'string':
        case 'number':
        case 'boolean':
        case 'bigint':
        case 'symbol':
        case 'undefined':
          if (objectSearchChecked) {
            break;
          }
          // Depending on logical AND or OR, results will be added
          if (
            (switchChecked &&
              search.every(
                (el) =>
                  key.toLowerCase().includes(el) ||
                  value.toString().toLowerCase().includes(el)
              )) ||
            (!switchChecked &&
              search.some(
                (el) =>
                  key.toLowerCase().includes(el) ||
                  value.toString().toLowerCase().includes(el)
              ))
          ) {
            results = [
              ...results,
              {
                path: newPath,
                value: value,
                displayPath: newDisplayPath,
              } as SearchResult,
            ];
          }
          break;
        case 'object':
          if (
            (switchChecked &&
              search.every(
                (el) =>
                  key.toLowerCase().includes(el) ||
                  nameIfExists?.toString().toLowerCase().includes(el)
              )) ||
            (!switchChecked &&
              search.some(
                (el) =>
                  key.toLowerCase().includes(el) ||
                  nameIfExists?.toString().toLowerCase().includes(el)
              ))
          ) {
            results = [
              ...results,
              {
                path: newPath,
                value: value,
                displayPath: newDisplayPath,
              } as SearchResult,
            ];
          } else {
            results = [
              ...results,
              ...SearchBrain(value as JsonArray, newPath, newDisplayPath),
            ];
          }
          break;
        default:
          throw DOMException;
      }
    }
    return results;
  };

  return (
    <div>
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        flexDirection="column"
      >
        <TextField
          id="searchbar"
          type="text"
          name="search"
          placeholder="Search through Torterra's Brain..."
          label="Combine Searchterms by separating with ';'"
          fullWidth
          onKeyDown={handleKeyDown}
          onChange={handleChange}
          sx={{ minWidth: '80vw' }}
        />
        <br />
        <FormControlLabel
          control={<Switch defaultChecked onChange={handleSwitchChange} />}
          label={'Combine Search with logical AND'}
        />
        <br />
        <FormControlLabel
          control={
            <Switch
              defaultChecked={false}
              onChange={handleObjectSearchChange}
            />
          }
          label={'Only search for objects'}
        />
        <br />
        <Button type="button" onClick={BeginSearch}>
          Search
        </Button>
        <Button type="button" onClick={ClearResults}>
          Clear Results
        </Button>
        <Button type="button" onClick={DisplayAll}>
          Display All
        </Button>
        <br />
      </Box>
      <Box
        display="flex"
        justifyContent="center"
        alignItems="flex-start"
        maxWidth="80vw"
      >
        <SearchResultList list={searchResults} />
      </Box>
    </div>
  );
};

export default SearchField;
