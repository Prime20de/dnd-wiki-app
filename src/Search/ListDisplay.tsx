import React from 'react';
import { SearchResult } from './SearchField';
import { ListItem, ListItemText } from '@mui/material';
import DataDisplay from './DataDisplay';
import { getKey, pathDisplay } from '../Utils/utils';

const ListDisplay = (props: SearchResult) => {
  if (typeof props.value != 'object') {
    return (
      <ListItem key={getKey()}>
        <ListItemText
          primary={pathDisplay(props.displayPath ?? props.path)}
          secondary={props.value}
        />
      </ListItem>
    );
  } else {
    return (
      <DataDisplay
        path={props.path}
        value={props.value}
        parent={pathDisplay(props.displayPath ?? props.path)}
      />
    );
  }
};

export default ListDisplay;
