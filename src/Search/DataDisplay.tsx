import React, { useState } from 'react';
import {
  Collapse,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import { Json, SearchResult } from './SearchField';
import { getKey, getTitle, isArray } from '../Utils/utils';
import { ChevronRight, ExpandMore } from '@mui/icons-material';

interface DataDisplayProps extends SearchResult {
  parent: string;
}

const DataDisplay = (props: DataDisplayProps) => {
  const [open, setOpen] = useState(false);
  const handleClick = () => setOpen(!open);

  const mainDisplay = () => (
    <ListItem button onClick={handleClick}>
      <ListItemIcon key={getKey()}>
        {open ? <ExpandMore /> : <ChevronRight />}
      </ListItemIcon>
      <ListItemText key={getKey()}>
        <b>{props.parent}</b>
      </ListItemText>
    </ListItem>
  );

  const collapsable = () => (
    <Collapse in={open} timeout={0}>
      <List component="div" sx={{ pl: 4 }}>
        {Object.keys(props.value).map((key) => {
          return props.value[key] != null &&
            typeof props.value[key] === 'object' ? (
            <DataDisplay
              key={getKey()}
              value={props.value[key] as Json}
              path={props.path}
              parent={getTitle(props.value[key] as Json, key)}
            />
          ) : (
            <ListItem key={getKey()}>
              <ListItemText
                primary={isArray(props.value) ? '' : key}
                secondary={props.value[key].toString()}
              />
            </ListItem>
          );
        })}
      </List>
    </Collapse>
  );

  return (
    <>
      {mainDisplay()}
      {collapsable()}
    </>
  );
};

export default DataDisplay;
