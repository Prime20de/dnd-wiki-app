import React from 'react';
import { SearchResult } from './SearchField';
import ListDisplay from './ListDisplay';
import { Divider, List } from '@mui/material';

interface SearchResultListProps {
  list: SearchResult[];
}

const SearchResultList = (props: SearchResultListProps) => {
  return (
    <List id="resultlist">
      {props.list.map((el, idx) => {
        return (
          <React.Fragment key={idx}>
            <ListDisplay
              path={el.path}
              value={el.value}
              displayPath={el.displayPath}
            />
            <Divider sx={{ marginTop: '5px', marginBottom: '5px' }} />
          </React.Fragment>
        );
      })}
    </List>
  );
};

export default React.memo(SearchResultList);
