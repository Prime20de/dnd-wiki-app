import React from 'react';
import PageTitle from '../Utils/PageTitle';
import SearchField from './SearchField';

const SearchPage = () => {
  return (
    <div>
      <PageTitle title={"K1 Torterra's Wiki"} />
      <SearchField />
    </div>
  );
};

export default SearchPage;
