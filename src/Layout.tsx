import React from 'react';
import {
  AppBar,
  Box,
  createTheme,
  CssBaseline,
  Drawer,
  styled,
  ThemeProvider,
  Toolbar,
  Typography,
} from '@mui/material';
import SearchPage from './Search/SearchPage';
import { default as CustomDrawer } from './Drawer/Drawer';

const theme = createTheme({
  palette: {
    mode: 'dark',
  },
});

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  alignItems: 'flex-start',
  paddingTop: theme.spacing(1),
  paddingBottom: theme.spacing(2),
  // Override media queries injected by theme.mixins.toolbar
  '@media all': {
    minHeight: 64,
  },
}));

const Layout = () => {
  return (
    <React.StrictMode>
      <div>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Box sx={{ display: 'flex' }}>
            <AppBar
              position="fixed"
              sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
            >
              <StyledToolbar>
                {/*<IconButton size="large" edge="start" color="inherit" aria-label="open drawer" sx={{ mr: 2 }}>*/}
                <Typography
                  variant="h5"
                  noWrap
                  component="div"
                  sx={{ flexGrow: 1, alignSelf: 'flex-end' }}
                >
                  Search
                </Typography>
                {/*</IconButton>*/}
              </StyledToolbar>
            </AppBar>
            <Drawer
              variant="permanent"
              sx={{
                width: 240,
                flexShrink: 0,
                [`& .MuiDrawer-paper`]: {
                  width: 240,
                  boxSizing: 'border-box',
                },
              }}
            >
              <Toolbar />
              <CustomDrawer />
            </Drawer>
            <main>
              <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                minHeight="10vh"
                marginLeft="5%"
              >
                <SearchPage />
              </Box>
            </main>
          </Box>
        </ThemeProvider>
      </div>
    </React.StrictMode>
  );
};

export default Layout;
