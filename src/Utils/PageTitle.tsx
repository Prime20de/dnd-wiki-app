import React from 'react';
import { Box } from '@mui/material';

interface TitleProps {
  title: string;
}

const Title = (props: TitleProps) => (
  <Box display="flex" justifyContent="center" alignItems="center">
    <h1>{props.title}</h1>
  </Box>
);

export default Title;
