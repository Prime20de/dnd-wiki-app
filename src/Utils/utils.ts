import { Json, JsonArray } from '../Search/SearchField';

export const getKey = () => Math.random().toString(36).substring(2, 9);

export const isArray = (item: Json | JsonArray) => Array.isArray(item);

export const pathDisplay = (path: string[]) => path.join(' - ');

export const getTitle = (value: Json, key: string) =>
  Object.keys(value).includes('Name') ? (value['Name'] as string) : key;
