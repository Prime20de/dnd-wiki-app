import {
  Box,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  SpeedDialIcon,
} from '@mui/material';

const Drawer = () => {
  // TODO: Get the overview about all files in ../../data/ after converting to server-side rendering
  return (
    <Box sx={{ overflow: 'auto' }}>
      <List>
        <ListItem button key={'test'}>
          <ListItemIcon>
            <SpeedDialIcon />
          </ListItemIcon>
          <ListItemText primary={'FileName'} />
        </ListItem>
        <p
          style={{
            margin: 'auto',
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
          }}
        >
          FileOverview
        </p>
        <Divider />
      </List>
    </Box>
  );
};

export default Drawer;
